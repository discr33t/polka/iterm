# iterm

Configure Opinionated iTerm setup

## Role Variables

```
iterm:
  tab_dir: ${HOME}
  window_dir: ${HOME}
  working_dir: ${HOME}
  start_text: '/usr/local/bin/tmux'
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.iterm

## License

MIT
